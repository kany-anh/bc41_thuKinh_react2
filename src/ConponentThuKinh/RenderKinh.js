import React, { Component } from 'react'

export default class RenderKinh extends Component {
    renderGlasses = () => {
        return (
            <div>
                {this.props.list.map((item, index) => {
                    let { price, name, url } = item;
                    return (
                        <img
                            onClick={() => {
                                this.props.thayDoiKinh(item);
                            }}
                            className="m-2 col-3 btn"
                            style={{ width: "20%", cursor: "pointer" }}
                            key={index}
                            src={url}
                        />
                    );
                })}
            </div>
        );
    };
    render() {
        return (
            <div className="container vglasses py-3">
                <div className="row container mt-5">
                    <div className="col-12">
                        <h1 className="mb-2">Virtual Glasses</h1>
                    </div>
                </div>
                <div className="row" id="vglassesList">
                    {this.renderGlasses()}
                </div>
            </div>
        )
    }
}
