import React, { Component } from "react";
import { dataKinh } from "./data";
import RenderKinh from "./RenderKinh";

class Select_Kinh extends Component {
  state = {
    glasses: {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./img/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    list: dataKinh,
  };
  changeGlasses = (newGlasses) => {
    this.setState({ glasses: newGlasses });
  };

  render() {
    return (
      <div
        style={{
          backgroundImage: "url(./img/background.jpg)",
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div
          style={{
            backgroundColor: "rgba(0,0,0,.5)",
            backgroundSize: "cover",
            minHeight: "1000px",
          }}
        >
          <h3
            style={{ backgroundColor: "rgba(199, 138, 138, 1)" }}
            className="text-center text-light p-5"
          >
            TRY GLASSES APP ONLINE
          </h3>
          <div className="container">
            <div className=" vglasses py-3">
              <div className="row  justify-content-between">
                <div className="col-6 vglasses__left ">
                  <div className="vglasses__card">
                    <div className="vglasses__model" id="avatar">
                      <img className="" src={this.state.glasses.url} alt="" />
                    </div>
                    <div id="glassesInfo" className="vglasses__info">
                      <h3 className="text-left">{this.state.glasses.name}</h3>
                      <br />
                      <h5 className="text-left">{this.state.glasses.desc}</h5>
                    </div>
                  </div>
                </div>
                <div className="col-5 vglasses__right p-2">
                  <div className="vglasses__card">
                    <div className="mb-2 text-right mt-2 mr-2" />
                    <div className="vglasses__model" />
                  </div>
                </div>
              </div>
            </div>
            <RenderKinh thayDoiKinh={this.changeGlasses} list={this.state.list} />
          </div>
        </div>
      </div>
    );
  }
}

export default Select_Kinh;
